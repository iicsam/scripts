#! /bin/bash
#For most modern Linux OS systems, the file /etc/os-release is really becoming standard and is getting included in most OS. So inside your Bash script you can just include the file, and you will have access to all variables described here (for example: NAME, VERSION, ...)
. /root/teqip-variables  # Include the variables-file in script because entering token through standard input can be difficult and can cause errors.
if [ -f /etc/os-release ]; 
then
    . /etc/os-release      # Include the variables defined in the related to OS.
else
    echo "Your OS distribution is not Centos dist. Currently we do not provide script support for OS distributions other than Centos7"
    exit
fi
if [[ $ID != centos ]] || [[ $VERSION_ID != 7 ]]   # Check for the version of OS. If not centos 7 advise accordingly.
then
    echo "Your OS version is not centos 7. Currently we do not provide script support for OS distributions other than Centos7"
    exit
fi
echo "Your System Version is Centos-7."
yum_check=$(which yum)
echo $yum_check
if [[ $yum_check == *yum* ]]   # Check for the installation of yum repo.
then
    echo "yum is installed in the system"
else
    echo "Install Yum repo first in the system"
    exit            # if yum not installed break the script.
fi
yum update -y
yum install epel-release -y
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
yum install yum-utils -y
yum-config-manager --enable remi-php74
yum install httpd wget php php-mysqlnd php-mbstring php-gd php-intl php-curl php-soap php-xml php-imagick php-csv php-mcrypt php-bcmath php-zip php-opcache -y
sed -i 's/enabled=0/enabled=1/g' /etc/yum.repos.d/remi-php74.repo   # change enabled=0 to enabled=1 in remi-php74.repo file.
systemctl start httpd
systemctl status httpd
systemctl enable httpd
#systemctl start php-fpm
#systemctl enable php-fpm
cd /var/www     # go to /var/www for further steps
rm -rf vendor
rm -rf uims_teqip       # Due to some reason if script is to be run again then existence of this folder would cause the script to break again. If this folder doesnt exist this command wouldnt do anything.
rm -rf /var/www/html/teqip_new      # Due to some reason if script is to be run again then existence of this folder would cause the script to break again.
rm -rf composer.json        # same as above
rm -rf /etc/httpd/conf.d/vhosts
rm -rf /etc/httpd/conf.d/vhosts/apache.conf     #same as above
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"       # installing composer
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php      # installing composer
php -r "unlink('composer-setup.php');"      # installing composer
./composer.phar config --global --auth http-basic.testing-proj.repo.repman.io token $consumer_token_repman      # configure access token of my repman for www-uims_teqip and html-teqip_new packages
wget http://65.2.39.22/download/composer.json       # temporary composer.json because composer.phar cant run without composer.json. Also it contains the address of my repman.
./composer.phar require trial/uims_teqip:dev-master   # download package uims_teqip. It will be downloaded into /var/www/vendor/trial/uims_teqip
mv vendor/trial/uims_teqip .        # by composer.phar packages are downloaded into vendor/vendorname/packagename folder. Move them to desired location.
./composer.phar require trial/teqip_new:dev-master      # similar comments as above
mv vendor/trial/teqip_new /var/www/html/
rm -rf vendor       # we no longer need vendor in /var/www because we already got our packages
rm -rf composer.json        # we no longer need composer.json in /var/www because we already got our packages
cd /var/www/uims_teqip
rm -rf composer.json        # this composer.json was created temporarily for downloading package through composer.phar. Now we dont need it
mv rncomposer.json composer.json        # we restore our real composer.json file
../composer.phar config --global --auth http-basic.teqip.repo.vcs.samarth.ac.in token $consumer_token_samarth       # configure access token of samarth repman for uims/dependencies.
../composer.phar update     # update command to install all the uims and other required packages.
cd /var/www/uims_teqip/config
sed -i 's://.*::' db.php        # to remove comments in file, otherwise difficult to use sed command
sed -i 's://.*::' db_admission.php      # to remove comments in file, otherwise difficult to use sed command
sed -i 's://.*::' db_nt.php     # to remove comments in file, otherwise difficult to use sed command
sed -i 's://.*::' db_rec.php        # to remove comments in file, otherwise difficult to use sed command
sed -i 's://.*::' db_student.php        # to remove comments in file, otherwise difficult to use sed command
sed -i 's://.*::' db_uims.php       # to remove comments in file, otherwise difficult to use sed command
sed -i "/mysql:host/c 'dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname'," db.php      # Change the values in database files according to variables declared in teqip-variables
sed -i "/username/c 'username' => '$mysql_username'," db.php
sed -i "/password/c 'password' => '$mysql_password'," db.php
sed -i "/mysql:host/c 'dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname'," db_admission.php
sed -i "/username/c 'username' => '$mysql_username'," db_admission.php
sed -i "/password/c 'password' => '$mysql_password'," db_admission.php
sed -i "/mysql:host/c 'dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname'," db_nt.php
sed -i "/username/c 'username' => '$mysql_username'," db_nt.php
sed -i "/password/c 'password' => '$mysql_password'," db_nt.php
sed -i "/mysql:host/c 'dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname'," db_rec.php
sed -i "/username/c 'username' => '$mysql_username'," db_rec.php
sed -i "/password/c 'password' => '$mysql_password'," db_rec.php
sed -i "/mysql:host/c 'dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname'," db_student.php
sed -i "/username/c 'username' => '$mysql_username'," db_student.php
sed -i "/password/c 'password' => '$mysql_password'," db_student.php
sed -i "/mysql:host/c 'dsn' => 'mysql:host=$mysql_dsn;dbname=$dbname'," db_uims.php
sed -i "/username/c 'username' => '$mysql_username'," db_uims.php
sed -i "/password/c 'password' => '$mysql_password'," db_uims.php
fwallstat=$(systemctl is-active firewalld)      # store status of firewalld in fwallstat
echo "Status of firewall is $fwallstat"
case "$fwallstat" in
#case 1
    "active") echo "Adding 80 and 443 ports to allowed ports in firewalld"       # if firewall active then follow following commands
    firewall-cmd --list-all
    firewall-cmd --zone=public --add-port=80/tcp --permanent
    firewall-cmd --zone=public --add-port=443/tcp --permanent
    firewall-cmd --reload
    firewall-cmd --list-all;;
#case 2
    "inactive") echo "Nothing to do" ;;         #if firewall inactive nothing to do
esac
chown -R apache:apache /var/www/html/teqip_new/assets/
chown -R apache:apache /var/www/uims_teqip/runtime
selstat=$(getenforce)       # store status of selinux
echo "Status of Selinux is $selstat"
case "$selstat" in
    #case 1
    "Enforcing") echo "Modifying contexts of assets and runtime"
        chcon -R -t httpd_sys_rw_content_t /var/www/html/teqip_new/assets/
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/teqip_new/assets(/.*)?"
        chcon -R -t httpd_sys_rw_content_t /var/www/uims_teqip/runtime
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/uims_teqip/runtime(/.*)?" 
        setsebool -P httpd_can_network_connect_db 1
        ;;
    #case 2
        "Permissive") echo "Nothing to do" ;;
    #case 3
        "Disabled") echo "Nothing to do" ;;
esac
#cd /etc/php-fpm.d
#cp www.conf www.conf.dist  # backup in case of any issues
#sock_status=$(ls /var/run/php-fpm/ | grep .sock)            #To check whether unix socket already exists or not
#if [[ $sock_status == *sock* ]]
#then
#    echo "sock already exists"              # sock already exists
#    if [[ $sock_status != www.sock ]]       # since we in our httpd apache.conf file have used www.conf as unix sock. Here check whether same name .sock exists or not.
#    then
#        echo "Either rename the Unix sock in /var/run/php-fpm to www.sock or Change the name of sock file in httpd apache.conf same as that in /var/run/php-fpm"
#    fi
#fi
#systemctl stop php-fpm
#systemctl status php-fpm
#sed -i "/listen = 127.0.0.1:9000/c listen = /var/run/php-fpm/www.sock" www.conf     # Generate sock file.
#sed -i "/;listen.owner = nobody/c listen.owner = apache" www.conf
#sed -i "/;listen.group = nobody/c listen.group = apache" www.conf
#systemctl start php-fpm
#systemctl status php-fpm
echo "Do you want to configure Web server. Please select 1 for Yes and 2 for no"        # This process ahead will download model apache config file.
select webservchoice in yes no
do
    if [[ $webservchoice == "yes" ]] || [[ $webservchoice == "no" ]]
    then
        echo "you have selected $webservchoice choice for web server configuration"
        break
    else
        echo "Please enter 1 for yes and 2 for no"      # If proper choice not selected by user then this message will pop up
    fi
done
echo $webservchoice
if [[ $webservchoice == yes ]]
then
    cd /etc/httpd/conf.d
    touch demo.conf     # create a file conf.d folder
    echo "IncludeOptional conf.d/vhosts/*.conf" > demo.conf     # Include vhosts folder for configurations
    mkdir vhosts
    cd vhosts
    wget http://65.2.39.22/download/apache.conf         # This model apache.conf file is already stored at this location
    systemctl restart httpd
    echo "Do you want to configure ServerName and ServerAlias. Please select 1 for Yes and 2 for no"        # To configure ServerName and ServerAlias through user Input
    select servernameconf in yes no
    do
        if [[ $servernameconf == yes ]] || [[ $servernameconf == no ]]
        then
            echo "you have selected $servernameconf choice for web server configuration"
            break
        else
            echo "Please enter 1 for yes and 2 for no"
        fi
    done
    echo $servernameconf
    if [[ $servernameconf == yes ]]
    then
        echo "Enter ServerName"
        read apache_server_name;        # Read the server name by user
        echo "Enter ServerAlias"
        read apache_server_alias;       # Read the server alias by user
        sed -i "/ServerName/c ServerName $apache_server_name" apache.conf     # Add the servername in apache.conf file
        sed -i "/ServerAlias/c ServerAlias $apache_server_alias" apache.conf     # Add the serveralias in apache.conf file
        systemctl restart httpd
    fi
fi
