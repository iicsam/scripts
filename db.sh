#! /bin/bash
exec > >(tee -a /var/log/scriptdb.log) 2>&1
if [ -f /etc/os-release ];
then
    . /etc/os-release      # Include the variables defined in the related to OS.
else
    echo -e "\033[0;32mYour OS distribution is not Centos. Currently we do not provide script support for OS distributions other than Centos7.\033[0m"
    exit
fi
if [[ $ID != centos ]] || [[ $VERSION_ID != 7 ]]   # Check for the version of OS
then
    echo -e "\033[0;32mYour OS version is not centos 7. Currently we do not provide script support for OS distributions other than Centos7.\033[0m"
    exit
fi
echo -e "\033[0;32mYour Instance Version is Centos-7.\033[0m"
yum_check=$(which yum)
echo $yum_check
if [[ $yum_check == *yum* ]]   # Check for the installation of yum repo.
then
    echo -e "\033[0;35myum installed.\033[0m"
else
    echo -e "\033[0;32mInstall Yum repo first.\033[0m"
    exit
fi
loc=$(pwd)
yum update -y
yum remove mysql -y
yum remove mysql-* -y
rm -rf mysql80-community-release-el7-3.noarch.rpm
rm -rf teqip_new_database.sql
rm -rf /var/lib/mysql
rm -rf /etc/my.cnf.rpmsave
rm -rf /var/log/mysqld.log
yum install wget -y
wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
yum install mysql80-community-release-el7-3.noarch.rpm -y
sed -i '21 s/enabled=0/enabled=1/' /etc/yum.repos.d/mysql-community.repo     # Yet to be done through pattern match.
sed -i '28 s/enabled=1/enabled=0/' /etc/yum.repos.d/mysql-community.repo
yum install mysql-server -y
systemctl start mysqld
systemctl status mysqld
systemctl enable mysqld
wget https://teqip.samarth.edu.in/download/teqip_new_database.sql
echo "sql_mode = \"STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION\"" >> /etc/my.cnf
echo -e "\033[0;32mAre the Database Server and Application server on same Instance?\033[0m"      # Check whether db server is on the same machine as that of App server
select servercond in yes no
do
    if [[ $servercond == "yes" ]] || [[ $servercond == "no" ]]
    then
        echo -e "\033[0;35mYou have selected $servercond choice.\033[0m"
        break
    else
        echo -e "\033[0;32mPlease enter 1 for yes and 2 for no.\033[0m"
    fi
done
if [[ $servercond == no ]]      # If Database server and app-server are not on same machine then we need to allow port 3306 in firewall for db connection.
then
    echo -e "\033[0;35mApplication Server and Database Server are not on same Instance.\033[0m"
    fwallstatdb=$(systemctl is-active firewalld)
    echo $fwallstatdb
    case "$fwallstatdb" in
    #case 1
        "active") echo -e "\033[0;35mfirewall active condition working.\033[0m"
        firewall-cmd --list-all
        firewall-cmd --zone=public --add-port=3306/tcp --permanent      # To add port 3306 in firewall
        firewall-cmd --reload
        firewall-cmd --list-all;;
    #case 2
        "inactive") echo "Nothing to do" ;;
    esac
fi
#grep 'temporary password' /var/log/mysqld.log

yum install expect -y    # required for automatic mysql_secure_installation script
echo -e "\033[0;32mEnter the Password to be set for root user. Password should be atleast 8 characters long and must contain atleast one Special Character, One Capital letter and One small letter.\033[0m"
read MYSQL_ROOT_PASSWORD;
echo -e "\033[0;32mYou have entered: $MYSQL_ROOT_PASSWORD as root user Password. Please note and store the Password in a secured place.\033[0m"
echo -e "\033[0;35mPlease be patient. Next process will take some time\033[0m"
MYSQL=$(grep 'temporary password' /var/log/mysqld.log | awk '{print $11}')   # used script of someone else
touch $loc/config.cnf
echo -e "[client]\nuser = \"root\"\npassword = \"$MYSQL_ROOT_PASSWORD\"\nhost = \"localhost\"" > $loc/config.cnf

SECURE_MYSQL=$(expect -c "

set timeout 10
spawn mysql_secure_installation

expect \"Enter password for user root:\"
send \"$MYSQL\r\"
expect \"New password:\"
send \"$MYSQL_ROOT_PASSWORD\r\"
expect \"Re-enter new password:\"
send \"$MYSQL_ROOT_PASSWORD\r\"
expect \"Change the password for root ?\ ((Press y\|Y for Yes, any other key for No) :\"
send \"y\r\"
send \"$MYSQL\r\"
expect \"New password:\"
send \"$MYSQL_ROOT_PASSWORD\r\"
expect \"Re-enter new password:\"
send \"$MYSQL_ROOT_PASSWORD\r\"
expect \"Do you wish to continue with the password provided?\(Press y\|Y for Yes, any other key for No) :\"
send \"y\r\"
expect \"Remove anonymous users?\(Press y\|Y for Yes, any other key for No) :\"
send \"y\r\"
expect \"Disallow root login remotely?\(Press y\|Y for Yes, any other key for No) :\"
send \"n\r\"
expect \"Remove test database and access to it?\(Press y\|Y for Yes, any other key for No) :\"
send \"y\r\"
expect \"Reload privilege tables now?\(Press y\|Y for Yes, any other key for No) :\"
send \"y\r\"
expect eof
")
echo $SECURE_MYSQL
echo -e "\033[0;32mDefault database name is teqip_new. Do you want to have different database name? Press 1 for yes and 2 for no.\033[0m"
select db_name_new in yes no
do
    if [[ $db_name_new == "yes" ]] || [[ $db_name_new == "no" ]]
    then
        echo -e "\033[0;35mYou have selected $db_name_new choice for Database Configuration. Please note the Database name at a secured place.\033[0m"
        break
    else
        echo -e "\033[0;32mPlease enter 1 for yes and 2 for no.\033[0m"      # If proper choice not selected by user then this message will pop up
    fi
done
dbname="teqip_new"
if [[ $db_name_new == yes ]]
then
    echo -e "\033[0;32mEnter the New Name of the Database.\033[0m"
    read dbname;
    echo -e "\033[0;35mYou have entered $dbname as new Database name.\033[0m"
    sed -i 's/teqip_new/'$dbname'/g' teqip_new_database.sql
fi
mysql --defaults-extra-file=$loc/config.cnf < teqip_new_database.sql
if [[ $servercond == no ]]
then
    echo -e "\033[0;35mSince Application Server and Database Server are not on same instance, You need to create another Database User.\033[0m"
    echo -e "\033[0;32mEnter the Name of the Database User you wish to create.\033[0m"
    read username;
    echo -e "\033[0;32mEnter the Password to be set for $username User. Password should be atleast 8 characters long and must contain atleast one Special Character, One Capital letter and One small letter.\033[0m"
    read passwd;
    echo -e "\033[0;35m You have entered $username as User and $passwd as Password. Please note and store it at secured place.\033[0m"
    mysql --defaults-extra-file=$loc/config.cnf -e "CREATE USER '$username'@'localhost' IDENTIFIED BY '$passwd';"
    mysql --defaults-extra-file=$loc/config.cnf -e "GRANT ALL PRIVILEGES ON *.* TO '$username'@'%' IDENTIFIED BY '$passwd';"
    mysql --defaults-extra-file=$loc/config.cnf -e "FLUSH PRIVILEGES;"
    echo -e "\033[0;32mNow You have following Database Users in MySql Server.\033[0m"
    mysql --defaults-extra-file=$loc/config.cnf -e "SELECT user FROM mysql.user;"
fi
echo -e "\033[0;32mPlease Enter your Organization name.\033[0m"
read org_name;
mysql --defaults-extra-file=$loc/config.cnf -D $dbname -e "UPDATE application_control SET application_control.value = '$org_name' WHERE application_control.key = 'org_name'";
random=$(mysql --defaults-extra-file=$loc/config.cnf -e "(SELECT CONCAT('TESTUNIVERSITY',SUBSTRING(MD5(RAND()),1,20)));")
bc=$(echo $random | awk '{print $2}')
echo $bc
mysql --defaults-extra-file=$loc/config.cnf -D $dbname -e "UPDATE application_control SET application_control.value = '$bc' WHERE application_control.key = 'hash_key'";

echo -e "\033[0;32mDo you plan to configure web server through Application Script on Application Server? Press 1 for yes and 2 for no.\033[0m"
select web_choice in yes no
do
    if [[ $web_choice == "yes" ]] || [[ $web_choice == "no" ]]
    then
        echo -e "\033[0;35mYou have selected $web_choice choice for Web Server Configuration through Application Script on Application Server.\033[0m"
        break
    else
        echo -e "\033[0;32mPlease enter 1 for yes and 2 for no.\033[0m"      # If proper choice not selected by user then this message will pop up
    fi
done
if [[ $servercond == no ]]      # If Database server and app-server are not on same machine then we need to allow port 3306 in firewall for db connection.
then
    echo -e "\033[0;32mPlease enter the I.P. Address of your Application Server so that application_control table can be updated accordingly.\033[0m"
    read IPADD;
    if [[ $web_choice == yes ]]
    then
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, '172.16.0.108/teqip_prod' , '$IPADD');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, 'xxx.xx.x.xxx/teqip_prod' , '$IPADD');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, 'teqip_prod' , 'teqip_new');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, '172.16.0.108' , '$IPADD');"
    else
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, '172.16.0.108/teqip_prod' , '$IPADD/teqip_new');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, 'xxx.xx.x.xxx/teqip_prod' , '$IPADD/teqip_new');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, 'teqip_prod' , 'teqip_new');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, '172.16.0.108' , '$IPADD');"
    fi
else
    if [[ $web_choice == yes ]]
    then
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, '172.16.0.108/teqip_prod' , '127.0.0.1');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, 'xxx.xx.x.xxx/teqip_prod' , '127.0.0.1');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, 'teqip_prod' , 'teqip_new');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, '172.16.0.108' , '127.0.0.1');"
    else
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, '172.16.0.108/teqip_prod' , '127.0.0.1/teqip_new');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, 'xxx.xx.x.xxx/teqip_prod' , '127.0.0.1/teqip_new');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, 'teqip_prod' , 'teqip_new');"
        mysql --defaults-extra-file=$loc/config.cnf -e "USE $dbname;UPDATE application_control SET value = replace(value, '172.16.0.108' , '127.0.0.1');"
    fi
fi
rm -rf $loc/config.cnf