#! /bin/bash
folder_www=
folder_html=
yum install git unzip -y
cd /var/www/html/$folder_html
rm -f mhrd.png
rm -f logo.png
wget https://du.samarth.ac.in/mhrd.png
wget https://du.samarth.ac.in/logo.png
yum install unzip -y
cd /var/www
rm -rf repo-sync-teqip
mkdir repo-sync-teqip
cd /var/www/repo-sync-teqip
wget https://teqip.samarth.edu.in/uims_teqip_updated.zip
unzip uims_teqip_updated.zip
mv uims_teqip_updated updated_uims
cd /var/www/
rm -rf /var/www/$folder_www/models
cp -R /var/www/repo-sync-teqip/updated_uims/models /var/www/$folder_www/models
rm -rf /var/www/$folder_www/controllers
cp -R /var/www/repo-sync-teqip/updated_uims/controllers /var/www/$folder_www/controllers
rm -rf /var/www/$folder_www/views
cp -R /var/www/repo-sync-teqip/updated_uims/views /var/www/$folder_www/views
rm -rf /var/www/$folder_www/themes
cp -R /var/www/repo-sync-teqip/updated_uims/themes /var/www/$folder_www/themes
rm -rf /var/www/$folder_www/components
cp -R /var/www/repo-sync-teqip/updated_uims/components /var/www/$folder_www/components
rm -rf /var/www/$folder_www/modules
cp -R /var/www/repo-sync-teqip/updated_uims/modules /var/www/$folder_www/modules
rm -f /var/www/$folder_www/config/config/console.php
cp /var/www/repo-sync-teqip/updated_uims/config/console.php /var/www/$folder_www/config/console.php
rm -f /var/www/$folder_www/config/config/migration.php
cp /var/www/repo-sync-teqip/updated_uims/config/migration.php /var/www/$folder_www/config/migration.php
rm -f /var/www/$folder_www/composer.json
cp /var/www/repo-sync-teqip/updated_uims/composer.json /var/www/$folder_www/composer.json
cd /var/www/$folder_www
../composer.phar update

cd /var/www/repo-sync-teqip/
../composer.phar create-project samarth/alumni-teqip alumni --repository-url=https://teqip.repo.vcs.samarth.ac.in
cd /var/www/repo-sync-teqip/alumni
cp -R conf/ config
random_string_1=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 25 | head -n 1)
echo $random_string_1
sed -i "s/'Enter your key here'/'$random_string_1'/" config/web.php
rm -rf /var/www/html/teqip_alumni
rm -rf /var/www/uims_alumni
cd /var/www/repo-sync-teqip
mv alumni/web /var/www/html/teqip_alumni
mv alumni /var/www/uims_alumni
cd /var/www/uims_alumni/config
rm -f db.php db_uims.php
cp /var/www/$folder_www/config/db.php db.php
cp db.php db_uims.php


cd /var/www/repo-sync-teqip/
../composer.phar create-project samarth/endowment-teqip endowment-public --repository-url=https://teqip.repo.vcs.samarth.ac.in
cd /var/www/repo-sync-teqip/endowment-public
cp -R conf/ config
random_string_2=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 25 | head -n 1)
echo $random_string_2
sed -i "s/'Enter your key here'/'$random_string_2'/" config/web.php 
rm -rf /var/www/html/teqip_endowment
rm -rf /var/www/uims_endowment
cd /var/www/repo-sync-teqip
mv endowment-public/web /var/www/html/teqip_endowment
mv endowment-public /var/www/uims_endowment
cd /var/www/uims_endowment/config
rm -f db.php db_uims.php
cp /var/www/$folder_www/config/db.php db.php
cp db.php db_uims.php

mkdir /var/www/html/teqip_alumni/assets/
mkdir /var/www/uims_alumni/runtime
chown -R apache:apache /var/www/html/teqip_alumni/assets/
chown -R apache:apache /var/www/uims_alumni/runtime

mkdir /var/www/html/teqip_endowment/assets/
mkdir /var/www/uims_endowment/runtime
chown -R apache:apache /var/www/html/teqip_endowment/assets/
chown -R apache:apache /var/www/uims_endowment/runtime

selstat=$(getenforce)       # store status of selinux
echo -e "\033[0;35mStatus of Selinux is $selstat\033[0m"
case "$selstat" in
    #case 1
    "Enforcing") echo -e "\033[0;35mModifying contexts of assets and runtime\033[0m"
        chcon -R -t httpd_sys_rw_content_t /var/www/html/teqip_alumni/assets/
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/teqip_alumni/assets(/.*)?"
        chcon -R -t httpd_sys_rw_content_t /var/www/uims_alumni/runtime
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/uims_alumni/runtime(/.*)?"
        chcon -R -t httpd_sys_rw_content_t /var/www/html/teqip_endowment/assets/
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/teqip_endowment/assets(/.*)?"
        chcon -R -t httpd_sys_rw_content_t /var/www/uims_endowment/runtime
        semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/uims_endowment/runtime(/.*)?" 
        setsebool -P httpd_can_network_connect_db 1
        ;;
    #case 2
        "Permissive") echo "Nothing to do" ;;
    #case 3
        "Disabled") echo "Nothing to do" ;;
esac

cd /var/www/$folder_www/
php yii migrate-alumni
php yii migrate-endowment
cd /var/www/html/teqip_alumni
rm -f index.php itdebug.php
curl https://bitbucket.org/devopsricha/scripts-1/raw/HEAD/alumni_index.php --output index.php
curl https://bitbucket.org/devopsricha/scripts-1/raw/HEAD/alumni_itdebug_index.php --output itdebug.php
cd /var/www/html/teqip_endowment
rm -f index.php itdebug.php
curl https://bitbucket.org/devopsricha/scripts-1/raw/HEAD/endowment_index.php --output index.php
curl https://bitbucket.org/devopsricha/scripts-1/raw/HEAD/endowment_itdebug_index.php --output itdebug.php